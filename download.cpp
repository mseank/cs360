/*
 * download.cpp
 *
 *  Created on: Jan 28, 2015
 *      Author: mseank
 */

#include <string.h>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#define SOCKET_ERROR        -1
#define BUFFER_SIZE         100
#define HOST_NAME_SIZE      255
#define MAX_MSG_SZ      1024

using namespace std;

//given code, sample.cpp
// Determine if the character is whitespace
bool isWhitespace(char c)
{ switch (c)
    {
        case '\r':
        case '\n':
        case ' ':
        case '\0':
            return true;
        default:
            return false;
    }
}

// Strip off whitespace characters from the end of the line
void chomp(char *line)
{
    int len = strlen(line);
    while (isWhitespace(line[len]))
    {
        line[len--] = '\0';
    }
}

// Read the line one character at a time, looking for the CR
// You dont want to read too far, or you will mess up the content
char * GetLine(int fds)
{
    char tline[MAX_MSG_SZ];
    char *line;

    int messagesize = 0;
    int amtread = 0;
    while((amtread = read(fds, tline + messagesize, 1)) < MAX_MSG_SZ)
    {
        if (amtread > 0)
            messagesize += amtread;
        else
        {
            perror("Socket Error is:");
            fprintf(stderr, "Read Failed on file descriptor %d messagesize = %d\n", fds, messagesize);
            exit(2);
        }
        //fprintf(stderr,"%d[%c]", messagesize,message[messagesize-1]);
        if (tline[messagesize - 1] == '\n')
            break;
    }
    tline[messagesize] = '\0';
    chomp(tline);
    line = (char *)malloc((strlen(tline) + 1) * sizeof(char));
    strcpy(line, tline);
    //fprintf(stderr, "GetLine: [%s]\n", line);
    return line;
}

// Change to upper case and replace with underlines for CGI scripts
void UpcaseAndReplaceDashWithUnderline(char *str)
{
    int i;
    char *s;

    s = str;
    for (i = 0; s[i] != ':'; i++)
    {
        if (s[i] >= 'a' && s[i] <= 'z')
            s[i] = 'A' + (s[i] - 'a');

        if (s[i] == '-')
            s[i] = '_';
    }

}


// When calling CGI scripts, you will have to convert header strings
// before inserting them into the environment.  This routine does most
// of the conversion
char *FormatHeader(char *str, char *prefix)
{
    char *result = (char *)malloc(strlen(str) + strlen(prefix));
    char* value = strchr(str,':') + 2;
    UpcaseAndReplaceDashWithUnderline(str);
    *(strchr(str,':')) = '\0';
    sprintf(result, "%s%s=%s", prefix, str, value);
    return result;
}

// Get the header lines from a socket
//   envformat = true when getting a request from a web client
//   envformat = false when getting lines from a CGI program

void GetHeaderLines(vector<char *> &headerLines, int skt, bool envformat)
{
    // Read the headers, look for specific ones that may change our responseCode
    char *line;
    char *tline;

    tline = GetLine(skt);
    while(strlen(tline) != 0)
    {
        if (strstr(tline, "Content-Length") ||
            strstr(tline, "Content-Type"))
        {
            if (envformat)
                line = FormatHeader(tline, "");
            else
                line = strdup(tline);
        }
        else
        {
            if (envformat)
                line = FormatHeader(tline, "HTTP_");
            else
            {
                line = (char *)malloc((strlen(tline) + 10) * sizeof(char));
                sprintf(line, "HTTP_%s", tline);
            }
        }
        //fprintf(stderr, "Header --> [%s]\n", line);

        headerLines.push_back(line);
        free(tline);
        tline = GetLine(skt);
    }
    free(tline);
}

int main(int argc, char* argv[])
{
    int fd;
	unsigned nReadAmount;
	int hSocket;
	string hostName;
	int portNum;
	string url;
	string tag;
	int downloadCount;
	hostent* server;
    sockaddr_in serv_addr;  /* Internet socket address stuct */
    long nHostAddress;
    char pBuffer[BUFFER_SIZE];

	hSocket = socket(AF_INET, SOCK_STREAM, 0);

	if(hSocket == SOCKET_ERROR)
	{
    	printf("\nCould not make a socket\n");
    	exit(0);
	}

	if(argc == 4){
		hostName = argv[1];
		portNum = atoi(argv[2]);
		url = hostName + "/" + argv[3];
	}

	else if(argc == 5){
		tag = argv[1];
		hostName = argv[2];
		portNum = atoi(argv[3]);
		url = hostName + "/" + argv[4];
	}

	else if(argc == 6){
		tag = argv[1];
		downloadCount = atoi(argv[2]);
		hostName = argv[3];
		portNum = atoi(argv[4]);
		url = hostName + "/" + argv[5];
		cout << "Hello" << endl;
	}

	else{
		printf("You submitted an incorrect number of arguments \n");
		exit(0);
	}

	server = gethostbyname(hostName.c_str());
	if(server == NULL)
	{
		cerr << "ERROR: no such hostname" << std::endl;
		exit(0);
	}

    //serv_addr.sin_addr.s_addr=nHostAddress;
    memcpy((char*) server->h_addr,(char*) &serv_addr.sin_addr.s_addr,server->h_length);
	//serv_addr.sin_port = htons(portNum);
    //serv_addr.sin_family=AF_INET;

    //memcpy((char*) server->h_addr,(char*) &serv_addr.sin_addr.s_addr,server->h_length);

	bzero((char*) &serv_addr, sizeof(serv_addr)); //what is this doing?

	//cout << hSocket << " : the socket" << endl;

	int check = connect(hSocket, (struct sockaddr*)&serv_addr, sizeof(serv_addr));

	//cout << tag << endl;

    /* read from socket into buffer
    ** number returned by read() and write() is the number of bytes
    ** read or written, with -1 being that an error occured */

    //printf("\nReceived \"%s\" from server\n",pBuffer);
    /* write what we received back to the server */
    // write(hSocket,pBuffer,nReadAmount);





    //more given code from sample.cpp

     vector<char *> headerLines;
     char buffer[MAX_MSG_SZ];
     char contentType[MAX_MSG_SZ];


     printf("Web Program Tools Example\n\n");

     // This shows how you could use these tools to implement a web client
     // We will talk about how to use them for the server too

     // Open the file that will simulate the socket
     fd = open("sample.txt",O_RDONLY);
     if(fd < 0) {
           perror("open of sample.txt failed");
           exit(0);
     }

     // First read the status line
       char *startline = GetLine(fd);
     printf("Status line %s\n\n",startline);

     // Read the header lines
       GetHeaderLines(headerLines, fd , false);


     // Now print them out
     for (int i = 0; i < headerLines.size(); i++) {
       printf("[%d] %s\n",i,headerLines[i]);
       if(strstr(headerLines[i], "Content-Type")) {
                sscanf(headerLines[i], "Content-Type: %s", contentType);
       }
     }

     printf("\n=======================\n");
     printf("Headers are finished, now read the file\n");
     printf("Content Type is %s\n",contentType);
     printf("=======================\n\n");

     // Now read and print the rest of the file
     int rval;
     while((rval = read(hSocket,buffer,MAX_MSG_SZ)) > 0) {
           write(1,buffer,rval);
     }


     nReadAmount=read(hSocket,pBuffer,BUFFER_SIZE);

     string requestString = "GET " + url + " HTTP/1.0\r\nHost: " + hostName + "\r\n\r\n";
     //nReadAmount=read(hSocket,pBuffer,BUFFER_SIZE);
     int n = write(hSocket, requestString.c_str(), strlen(requestString.c_str()));

     cout << n << endl;

     printf("\nWriting \"%s\" to server",pBuffer);

     printf("\nClosing socket\n");
     /* close socket */
     if(close(hSocket) == SOCKET_ERROR)
     {
         printf("\nCould not close socket\n");
         return 0;
     }
}
